# README #


* Summary
Programming assignment for skills evaluation purposes

* Requeriments
Java 8
Maven 4
mysql 


* Dependencies
mysql
maven
swagger2
spring boot
jpa
junit


* Database configuration
/src/main/resource/application.properties


* API Resource documentation
{SERVICE_URL}/swagger-ui.html


* How to run tests
mvn test

* Deployment instructions
TODO

* Improvements
Test coverage must be incremented
Validation over transactions 
Request Authentication