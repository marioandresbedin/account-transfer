package com.accountbalance.repository;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accountbalance.app.ApiConstants;
import com.accountbalance.model.TransferRequest;

@Repository
@Transactional(readOnly = false)
public class TransferRequestRepositoryImpl implements TransferRequestCustomRepository {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public TransferRequest transferMoney(TransferRequest trRequest) {
		trRequest.getAccountTransferFrom()
				.setBalance(trRequest.getAccountTransferFrom().getBalance().subtract(trRequest.getTransferAmmount()));
		trRequest.getAccountTransferFrom().setUpdateTime(new Date());
		trRequest.getAccountTransferReceiver()
				.setBalance(trRequest.getAccountTransferReceiver().getBalance().add(trRequest.getTransferAmmount()));
		trRequest.getAccountTransferReceiver().setUpdateTime(new Date());
		trRequest.setStatus(ApiConstants.COMPLETE_STATUS);
		trRequest.setTransferDate(new Date());
		entityManager.persist(trRequest);
		return trRequest;
	}

}
