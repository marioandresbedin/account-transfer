package com.accountbalance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.accountbalance.model.TransferRequest;

@RepositoryRestResource(collectionResourceRel = "transferRequest", path = "transferRequest")
public interface TransferRequestRepository extends JpaRepository<TransferRequest, Long>, TransferRequestCustomRepository {

	
}
