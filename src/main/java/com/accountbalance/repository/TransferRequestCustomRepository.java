package com.accountbalance.repository;

import com.accountbalance.model.TransferRequest;

public interface TransferRequestCustomRepository {
	
	TransferRequest transferMoney(TransferRequest trRequest);

}
