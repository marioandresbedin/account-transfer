package com.accountbalance.app;

public class ApiConstants {

	private ApiConstants() {
	}

	public static final String PENDING_STATUS = "TRANSACTION_PENDING";
	public static final String COMPLETE_STATUS = "TRANSACTION_COMPLETE";
	public static final String DEFAULT_CURRENCY = "EUR";

}
