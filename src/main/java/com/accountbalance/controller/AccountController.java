package com.accountbalance.controller;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.accountbalance.app.ApiConstants;
import com.accountbalance.model.Account;
import com.accountbalance.repository.AccountRepository;
import com.accountbalance.util.CustomErrorType;

@RestController
@RequestMapping("/account")
public class AccountController {

	public static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountRepository accountRepository;

	@RequestMapping(value = "/{accountNumber}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Account findAccountByAccountNumber(@PathVariable("accountNumber") String accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Account> getAccounts() {
		return accountRepository.findAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> createAcount(@RequestBody Account account, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Account : {}", account);

		if (accountRepository.findByAccountNumber(account.getAccountNumber()) != null) {
			logger.error("Unable to create. An account with account number {} already exist", account.getAccountNumber());
			return new ResponseEntity<Object>(new CustomErrorType("Unable to create. A User with name " + 
			account.getAccountNumber() + " already exist."), HttpStatus.CONFLICT);
		}
		Calendar cal = Calendar.getInstance();
		account.setCreateTime(cal.getTime());
		account.setUpdateTime(cal.getTime());
		account.setCurrency(ApiConstants.DEFAULT_CURRENCY);
		accountRepository.save(account);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(
				ucBuilder.path("/account/{accountNumber}").buildAndExpand(account.getAccountNumber()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

}
