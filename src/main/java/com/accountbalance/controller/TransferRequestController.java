package com.accountbalance.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.accountbalance.app.ApiConstants;
import com.accountbalance.model.Account;
import com.accountbalance.model.AccountTransferRequest;
import com.accountbalance.model.TransferRequest;
import com.accountbalance.repository.AccountRepository;
import com.accountbalance.repository.TransferRequestRepository;
import com.accountbalance.util.CustomErrorType;

@RestController
@RequestMapping("/transfer-request")
public class TransferRequestController {

	public static final Logger logger = LoggerFactory.getLogger(TransferRequestController.class);

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransferRequestRepository transferRequestRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<TransferRequest> getTransferRequests() {
		return transferRequestRepository.findAll();
	}

	@RequestMapping(value = "/{transferId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TransferRequest findTransferById(@PathVariable("transferId") Long transferId) {
		return transferRequestRepository.findOne(transferId);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> createTransferRequest(@Valid @RequestBody AccountTransferRequest transferRequest,
			Errors errors) {
		logger.info("Transfering : {} from account: {} to destiny account: {}",
				new Object[] { transferRequest.getTransferAmmount(), transferRequest.getAccountTransferNumber(),
						transferRequest.getAccountReceiverNumber() });

		Account accountSource = accountRepository.findByAccountNumber(transferRequest.getAccountTransferNumber());
		if (accountSource == null) {
			return new ResponseEntity<Object>(
					new CustomErrorType(String.format("Unable to transfer money. Account %s doesnt exist. ",
							transferRequest.getAccountTransferNumber())),
					HttpStatus.NOT_FOUND);
		}

		Account accountReceiver = accountRepository.findByAccountNumber(transferRequest.getAccountReceiverNumber());
		if (accountReceiver == null) {
			return new ResponseEntity<Object>(
					new CustomErrorType(String.format("Unable to transfer money. Account %s doesnt exist. ",
							transferRequest.getAccountReceiverNumber())),
					HttpStatus.NOT_FOUND);
		}

		if (accountSource.getBalance().compareTo(transferRequest.getTransferAmmount()) < 0) {
			return new ResponseEntity<Object>(
					new CustomErrorType(String.format("Unable to transfer money. Account %s not has enough funds. ",
							transferRequest.getAccountTransferNumber())),
					HttpStatus.CONFLICT);
		}

		// Validations OK Preparing transfer request
		TransferRequest trRequest = new TransferRequest();
		trRequest.setAccountTransferFrom(accountSource);
		trRequest.setAccountTransferReceiver(accountReceiver);
		trRequest.setTransferAmmount(transferRequest.getTransferAmmount());
		trRequest.setTransferCurrency(ApiConstants.DEFAULT_CURRENCY);
		trRequest.setStatus(ApiConstants.PENDING_STATUS);
		trRequest = transferRequestRepository.transferMoney(trRequest);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

}
