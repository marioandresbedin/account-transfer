package com.accountbalance.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transfer_request")
public class TransferRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "account_transfer_from_id")
	private Account accountTransferFrom;

	@ManyToOne(optional = false)
	@JoinColumn(name = "account_transfer_receiver_id")
	private Account accountTransferReceiver;

	@Column(name = "transfer_ammount")
	private BigDecimal transferAmmount;

	@Column(name = "transfer_date")
	private Date transferDate;

	@Column(name = "status")
	private String status;

	@Column(name = "transfer_currency")
	private String transferCurrency;

	public Account getAccountTransferFrom() {
		return accountTransferFrom;
	}

	public void setAccountTransferFrom(Account accountTransferFrom) {
		this.accountTransferFrom = accountTransferFrom;
	}

	public Account getAccountTransferReceiver() {
		return accountTransferReceiver;
	}

	public void setAccountTransferReceiver(Account accountTransferReceiver) {
		this.accountTransferReceiver = accountTransferReceiver;
	}

	public BigDecimal getTransferAmmount() {
		return transferAmmount;
	}

	public void setTransferAmmount(BigDecimal transferAmmount) {
		this.transferAmmount = transferAmmount;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferCurrency() {
		return transferCurrency;
	}

	public void setTransferCurrency(String transferCurrency) {
		this.transferCurrency = transferCurrency;
	}

}
