package com.accountbalance.model;

import java.math.BigDecimal;

import org.hibernate.validator.constraints.NotBlank;

public class AccountTransferRequest {
	
    @NotBlank(message = "Account transfer number can't be empty!")
	private String accountTransferNumber;
	
    @NotBlank(message = "Account receiver number can't be empty!")
	private String accountReceiverNumber;
	
	private BigDecimal transferAmmount;

	public String getAccountTransferNumber() {
		return accountTransferNumber;
	}

	public void setAccountTransferNumber(String accountTransferNumber) {
		this.accountTransferNumber = accountTransferNumber;
	}

	public String getAccountReceiverNumber() {
		return accountReceiverNumber;
	}

	public void setAccountReceiverNumber(String accountReceiverNumber) {
		this.accountReceiverNumber = accountReceiverNumber;
	}

	public BigDecimal getTransferAmmount() {
		return transferAmmount;
	}

	public void setTransferAmmount(BigDecimal transferAmmount) {
		this.transferAmmount = transferAmmount;
	}
 

}
