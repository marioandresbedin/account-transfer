package accountbalance.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.accountbalance.app.ApiConstants;
import com.accountbalance.app.App;
import com.accountbalance.model.Account;
import com.accountbalance.model.TransferRequest;
import com.accountbalance.repository.AccountRepository;
import com.accountbalance.repository.TransferRequestRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@WebAppConfiguration
@Transactional
public class TransferRequestControllerTest {

	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	private MockMvc mockMvc;
	private Account accountSource;
	private Account accountReceiver;
	private TransferRequest transferRequest;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransferRequestRepository transferRequestRepository;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {
		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);
		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		accountRepository.deleteAllInBatch();
		transferRequestRepository.deleteAllInBatch();
		this.accountSource = accountRepository.save(new Account("1000", new BigDecimal(2500)));
		this.accountReceiver = accountRepository.save(new Account("1001", new BigDecimal(1700)));
		this.transferRequest = new TransferRequest();
		this.transferRequest.setTransferAmmount(new BigDecimal(500));
		this.transferRequest.setAccountTransferFrom(this.accountSource);
		this.transferRequest.setAccountTransferReceiver(this.accountReceiver);
		this.transferRequest.setStatus(ApiConstants.COMPLETE_STATUS);
		transferRequestRepository.save(this.transferRequest);
	}

	@Test
	@Rollback(true)
	public void getTransferRequests() throws Exception {
		mockMvc.perform(get("/transfer-request/")).andExpect(status().isOk());
	}

}
